<?php

	var $host = "localhost";
	var $dbname = "php-demo";

	try {
		$db = new PDO('mysql:host='. $host .';dbname='. $dbname , 'root', '');
	}
	catch(PDOException $e){
		echo 'ERROR    ';
	}

	$sql = 'SELECT title, description, url FROM posts';
	$req = $db->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP Demo</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <style>
		body {
		  padding-top: 70px;
		}
    </style>
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="starter-template">
        <h1>Bootstrap home page</h1>
        <p class="lead"> foreach Not showinng the first element.</p>
      </div>
		<?php while($result = $req->fetch(PDO::FETCH_ASSOC)){ ?>		
			<div class="row">
				<?php foreach($req as $post) { ?>
		            <div class="col-xs-6 col-lg-4">
		              <h2><?php echo $post["title"] ;?></h2>
		              <p><?php echo $post["description"] ;?></p>
		              <p><a class="btn btn-default" href="<?php echo $post["url"] ;?>" role="button">View details »</a></p>
		            </div><!--/.col-xs-6.col-lg-4-->
		        <?php } ?>    
	        </div>  
		<?php } ?>
    </div><!-- /.container -->
  </body>
</html>


